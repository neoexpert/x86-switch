package com.neoexpert.x86GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.*;

import com.neoexpert.x86.modes.X86;
import com.neoexpert.x86.AsmLog;
import com.neoexpert.x86.Screen;

import java.awt.*;

public class MainGUI implements ActionListener, Screen, AsmLog {
	final static boolean shouldFill = true;
	final static boolean shouldWeightX = true;
	final static boolean RIGHT_TO_LEFT = false;
	private X86 x86;

	private JTextArea textArea;
	private JButton stepButton;
	private JButton continueButton;
	private JButton resetButton;
	private JSpinner steps;

	public MainGUI(String file) throws FileNotFoundException, IOException {
		x86 = new X86(file, this);
		x86.s.setLog(this);
		x86.setScreen(this);
	}

	public void addComponentsToPane(Container pane) {
		if (RIGHT_TO_LEFT) {
			pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		regs.put("EIP", new Register(new JLabel("0")));
		regs.put("EIP", new Register(new JLabel("0")));
		regs.put("EIP", new Register(new JLabel("0")));
		regs.put("EAX", new Register(new JLabel("0")));
		regs.put("EBX", new Register(new JLabel("0")));
		regs.put("ECX", new Register(new JLabel("0")));
		regs.put("EDX", new Register(new JLabel("0")));
		regs.put("ESI", new Register(new JLabel("0")));
		regs.put("EDI", new Register(new JLabel("0")));
		regs.put("EBP", new Register(new JLabel("0")));
		regs.put("ESP", new Register(new JLabel("0")));
		regs.put("IP", new Register(new JLabel("0")));
		regs.put("EFLAGS", new Register(new JLabel("0")));
		regs.put("CS", new Register(new JLabel("0")));
		regs.put("DS", new Register(new JLabel("0")));
		regs.put("ES", new Register(new JLabel("0")));
		regs.put("SS", new Register(new JLabel("0")));
		regs.put("FS", new Register(new JLabel("0")));
		regs.put("GS", new Register(new JLabel("0")));
		regs.put("GDTR", new Register(new JLabel("0")));
		regs.put("IDTR", new Register(new JLabel("0")));
		regs.put("CR0", new Register(new JLabel("0")));
		regs.put("CR2", new Register(new JLabel("0")));
		regs.put("CR3", new Register(new JLabel("0")));
		regs.put("CR4", new Register(new JLabel("0")));
		regs.put("EFER", new Register(new JLabel("0")));

		Set<String> keys = regs.keySet();

		for (String key : keys) {
			Register reg = regs.get(key);

			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy++;
			pane.add(new JLabel(key), c);
			c.gridx = 1;

			pane.add(reg.label, c);
		}

		c.gridy++;

		SpinnerModel model = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
		steps = new JSpinner(model);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 40; // make this component tall
		c.weightx = 0.01;
		c.gridwidth = 1;
		c.gridx = 0;
		pane.add(steps, c);

		stepButton = new JButton("step");
		stepButton.setActionCommand("STEP");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 40; // make this component tall
		c.weightx = 1.0 / 4.0;
		c.gridwidth = 1;
		c.gridx = 1;
		pane.add(stepButton, c);
		stepButton.addActionListener(this);

		continueButton = new JButton("continue");
		continueButton.setActionCommand("CONTINUE");

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 40; // make this component tall
		c.weightx = 1.0 / 4.0;
		c.gridwidth = 1;
		c.gridx = 2;
		pane.add(continueButton, c);
		continueButton.addActionListener(this);

		resetButton = new JButton("reset");
		resetButton.setActionCommand("RESET");

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 40; // make this component tall
		c.weightx = 1.0 / 4.0;
		c.gridwidth = 1;
		c.gridx = 3;
		pane.add(resetButton, c);
		resetButton.addActionListener(this);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 40; // make this component tall
		c.weightx = 0.0;
		c.gridwidth = 4;
		c.gridx = 0;
		c.gridy++;
		textArea = new JTextArea(5, 80);
		textArea.setForeground(Color.WHITE);
		textArea.setBackground(Color.BLACK);

		textArea.setEditable(false);
		pane.add(textArea, c);
		;

	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked
	 * from the event-dispatching thread.
	 */
	private void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("x86");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Set up the content pane.
		addComponentsToPane(frame.getContentPane());

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		if (args.length < 1) {
			System.err.println("error: no input file");
			return;
		}
		MainGUI mg = new MainGUI(args[0]);
		if (args.length > 1) {
			if (args[1].equals("-v"))
				mg.x86.s.setLog(new AsmLog() {

					@Override
					public void log(String asm) {
						System.out.println(asm);
					}});
		}
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				mg.createAndShowGUI();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "STEP":
			Integer count = (Integer) steps.getValue();
			for (int i = 0; i < count; i++) {
				x86.step();
				refreshLabels();
			}

			break;
		case "CONTINUE":
			continueButton.setEnabled(false);
			stepButton.setEnabled(false);
			_continue();
			break;
		case "RESET":
			textArea.setText("");
			x86.reset();
			continueButton.setEnabled(true);
			stepButton.setEnabled(true);
			break;
		}

	}

	private void _continue() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					x86.run();
				}
				catch (Exception e) {
					e.printStackTrace();
				}

				continueButton.setEnabled(true);
				stepButton.setEnabled(true);

			}
		}).start();
	}

	public class Register {
		public Register(JLabel label) {
			this.label = label;
			label.setFont(new Font("monospaced", Font.PLAIN, 15));
		}

		public void setValue(String v) {
			if (v.equals(prevValue))
				label.setForeground(Color.BLACK);
			else
				label.setForeground(Color.RED);

			label.setText(v);
			prevValue = v;
		}

		public JLabel label;
		public String prevValue = "0";

	}

	Map<String, Register> regs = new LinkedHashMap<>();

	private void refreshLabels() {
		regs.get("EIP").setValue(x86.s.getEIPString());
		regs.get("EAX").setValue(x86.s.getEAXString());
		regs.get("EBX").setValue(x86.s.getEBXString());
		regs.get("ECX").setValue(x86.s.getECXString());
		regs.get("EDX").setValue(x86.s.getEDXString());
		regs.get("ESI").setValue(x86.s.getESIString());
		regs.get("EDI").setValue(x86.s.getEDIString());
		regs.get("EBP").setValue(x86.s.getEBPString());
		regs.get("ESP").setValue(x86.s.getESPString());
		regs.get("IP").setValue(x86.s.getIPString());
		regs.get("EFLAGS").setValue(x86.s.getEFLAGSString());
		regs.get("CS").setValue(x86.s.getCSString());
		regs.get("DS").setValue(x86.s.getDSString());
		regs.get("ES").setValue(x86.s.getESString());
		regs.get("SS").setValue(x86.s.getSSString());
		regs.get("FS").setValue(x86.s.getFSString());
		regs.get("GS").setValue(x86.s.getGSString());
		regs.get("GDTR").setValue(x86.s.getGDTRString());
		regs.get("IDTR").setValue(x86.s.getIDTRString());
		regs.get("CR0").setValue(x86.s.getCR0String());
		regs.get("CR2").setValue(x86.s.getCR2String());
		regs.get("CR3").setValue(x86.s.getCR3String());
		regs.get("CR4").setValue(x86.s.getCR4String());
		regs.get("EFER").setValue(x86.s.getEFERString());
	}

	@Override
	public void writeChar(char c) {
		textArea.append(c + "");
	}

	@Override
	public void log(String asm) {
		// TODO Auto-generated method stub
		
	}
}