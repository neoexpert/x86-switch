package com.neoexpert.x86.modes;

import com.neoexpert.x86.AsmLog;
import com.neoexpert.x86.Screen;
import com.neoexpert.x86.State;

import java.io.*;
import java.util.Scanner;

import static com.neoexpert.x86.State.DF;

public class X86{
	public static final int opsize = 2;
	public static final int addrsize = 2;

	public State s;

	public void loadImage(InputStream is, Screen screen) throws IOException {
		s = new State(16);
		s.setScreen(screen);
		is.read(s.ram, s.startpos, is.available());
		is.close();
		//s.r.order(ByteOrder.LITTLE_ENDIAN);

	}

	public X86(String filename, Screen screen) throws IOException {
		File file = new File(filename);
		InputStream is = new BufferedInputStream(new FileInputStream(file));

		loadImage(is, screen);

	}

	public X86(InputStream is, Screen screen) throws IOException {
		loadImage(is, screen);
	}

	public static void main(String[] args) {

		if (args.length < 1) {
			System.err.println("error: no input file");
			return;
		}
		Screen s = new Screen() {
			@Override
			public void writeChar(char c) {
				System.out.print(c);
			}
		};

		X86 x86;
		try {
			x86 = new X86(args[0], s);
		} catch (Exception e) {
			return;
		}

		boolean debug=false;
		for(int i=1;i<args.length;++i){
			String arg=args[i];
			switch(arg){
				case "-v":

					x86.s.setLog(new AsmLog() {

						@Override
						public void log(String asm) {
							System.out.println(asm);
						}

					});
					break;
				case "-d":
					debug=true;
					break;
			}
		}

		if(debug){
			Scanner scanner=new Scanner(System.in);
			// Read line

			String str;
			while((str = scanner.nextLine())!=null){
				x86.step();
				System.out.println();
				System.out.println(x86.s);
			}
		}
		else	
			while (true) {
				x86.run();
			}	

	}

	public void run() {
		while (true) {
			step(s);
		}
	}

	public void step() {
		step(s);
	}

	private void step(State s){
		int op = (s.ram[s.eip]);
		op&=0xff;

		byte param;
		int result = 0;

		int val;
		int tos;
		switch (op) {
		case 0x00:
			//s.r.position(++s.eip);
			++s.eip;
			++s.eip;
			s.logasm("add " + s.getOperand() + " " + s.getOperand());
			break;

		case 0x04:// Add imm8 to AL.
			s.EAX += s.ram[++s.eip];
			break;
		case 0x08:
			s.logasm("OR AL, AL ");
			s.setAL((byte) (s.getAL() | s.getAL()));
			param = s.ram[s.eip++];
			break;
		case 0x0f:
			_0f(s);
			break;
		case 0x10:
			s.logasm("adc " + s.byteToHex(s.ram[++s.eip]) + " " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0x16:
			s.logasm("push " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0x24:
			s.logasm("AND " + s.byteToHex(s.ram[++s.eip]) + " " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0x26:
			s.logasm("es ");
			break;
		case 0x29:
			param = s.ram[++s.eip];
			switch (param) {
			case (byte) 0xF6:
				s.logasm("SUB SI SI");
				s.setESI((s.getESI() - s.getESI()));

				break;
			case (byte) 0xFF:
				s.logasm("SUB DI DI");
				s.setEDI((s.getEDI() - s.getEDI()));
				break;
			default:
				s.logasm("SUB X,X param=" + s.byteToHex(param));

				break;
			}
			s.unsetFlag(s.OF);
			s.unsetFlag(s.CF);

			break;
		case 0x2E:
			s.logasm("cs " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0x31:
			param = s.ram[++s.eip];
			s.unsetFlag(s.OF);
			s.unsetFlag(s.CF);
			switch (param) {
			case (byte) 0xdb:
				s.logasm("XOR", "BX", "BX");
				result = s.setBX((s.getBX() ^ s.getBX()));
				result = s.getBX();
				break;
			case (byte) 0xc0:
				s.logasm("XOR", "AX", "AX");
				result = s.setAX((short)(s.getAX() ^ s.getAX()));

				break;
			default:
				s.logasm("XOR X,X param=" + s.byteToHex(param));

				break;
			}
			s.setFlags(result);

			break;
		case 0x40:
			s.logasm("org " + s.byteToHex(s.ram[++s.eip]) + " " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0x43:
			s.logasm("INC BX");
			s.EBX++;
			break;
		case 0x46:
			s.logasm("INC SI ");
			s.ESI++;
			break;
		case 0x50:
			s.logasm("PUSH AX");
			s.stackpush16(s.getAX());
			break;
		case 0x51:
			s.logasm("PUSH CX");
			s.stackpush16(s.getCX());
			break;
		case 0x52:
			s.logasm("PUSH DX");
			s.stackpush16(s.getDX());
			break;
		case 0x53:
			s.logasm("PUSH BX");
			s.stackpush16(s.getBX());

			break;
		case 0x58:
			s.logasm("POP AX");
			s.setAX(s.stackpop16());
			break;
		case 0x5B:
			s.logasm("POP BX");
			s.setBX(s.stackpop16());
			break;
		case 0x64:
			s.logasm("FS segment override prefix");
			break;
		case 0x66:
			s.logasm("Operand-size override prefix");
			s.eip++;
			s.toggleOpSize();
			step(s);
			s.toggleOpSize();
			return;
		case 0x67:
			s.logasm("Address-size override prefix");

			break;
		case 0x72:
			s.logasm("jb " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0x74:
			byte to = s.ram[++s.eip];
			if (s.getAL() == 0)
				s.eip += to;
			s.logasm("jz " + to);
			break;
		case 0x7a:
			s.logasm("jp " + s.byteToHex(s.ram[++s.eip]));
			break;
		
		case 0x81:
			param = s.ram[++s.eip];
			int param2 = s.ram[++s.eip];
			s.logasm("0x81 "+s.byteToHex(param)+ " "+s.byteToHex(param2));
			break;
		case 0x84:
			param = s.ram[++s.eip];
			switch (param) {
				case (byte) 0xc0:
					s.logasm("TEST AL,AL");
					result = (s.getAL() & s.getAL());

					break;
				default:
					s.logasm("TEST X,X param=" + s.byteToHex(param));

					break;
			}
			s.setFlags(result);
			break;
		case 0x88:
			param = s.ram[++s.eip];
			switch (param) {
			case (byte) 0x16:
				int ptr = s.getDS()*16+ s.get_i16(++s.eip);
				s.logasm("MOV ["+s.intToHex(ptr)+"],DL");
				s.eip++;
				s.set_i8(ptr, s.getDL());

				break;
			
			default:
				s.logasm("MOV X,X param=" + s.byteToHex(param));

				break;
			}

			break;
		case 0x89:
			param = s.ram[++s.eip];
			switch (param) {
			case (byte) 0xD7:
				s.logasm("MOV DI, DX");
				s.setDI(s.getDX());
				break;
			case (byte) 0xD4:
				s.logasm("MOV SP, DX");
				s.setSP(s.getDX());
				break;
			case (byte) 0xF7:
				s.logasm("MOV DI, SI");
				s.setDI(s.getSI());
				break;
			default:
				s.logasm("MOV X,X param=" + s.byteToHex(param));

				break;
			}

			break;
		case 0x8a:
			s.logasm("MOV AL" + s.byteToHex(s.ram[++s.eip]));
			s.setAL(s.ram[s.getSI()]);
			break;
		case 0x8c:
			param = s.ram[++s.eip];
			switch (param) {
			case (byte) 0xC8:
				s.logasm("MOV AX, CS");
				s.setAX(s.getCS());
				break;
			default:
				s.logasm("MOV AX, CX param=" + s.byteToHex(param));

				break;
			}

			break;
		case 0x8e:
			param = s.ram[++s.eip];
			switch (param) {
			case (byte) 0xC0:
				s.logasm("MOV ES, AX");
				s.setES(s.getAX());

				break;
			case (byte) 0xD8:
				s.logasm("MOV DS, AX");
				s.setDS(s.getAX());

				break;
			case (byte) 0xD0:
				s.logasm("MOV SS, AX");
				s.setSS(s.getAX());

				break;
			case (byte) 0xE0:
				s.logasm("MOV FS, AX");
				s.setFS(s.getAX());

				break;
			case (byte) 0xE8:
				s.logasm("MOV GS, AX");
				s.setGS(s.getAX());

				break;
			default:
				s.logasm("MOV X,AX param=" + s.byteToHex(param));

				break;
			}
			break;
		case 0xA8:
			s.logasm("test " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0xAA:
			s.logasm("STOSB");
			int addr = s.getES();
			s.ram[addr] = s.getAL();
			break;
		case 0xAB:
			s.logasm("STOSW");
			if ((s.EFLAGS & DF) == 0) 
				s.ESI++;// = AX; 
			else 
				s.ESI--;// = AX;
			s.set_i16(s.EDI, s.getAX());
			break;
		case 0xAC:
			s.logasm("LODSB");
			s.setAL(s.ram[s.DS&0xffff * 16 + s.getSI()]);
			if ((s.EFLAGS & DF) == 0) 
				s.ESI+=1;
			else 
				s.ESI-=1;
			break;
		case 0xAD:
			s.logasm("LODSW");
			s.setAX(s.get_i16(s.DS&0xffff * 16 + s.getSI()));
			if ((s.EFLAGS & DF) == 0) 
				s.ESI+=2;
			else 
				s.ESI-=2;
			break;
		case 0xB0:
			s.logasm("MOV AL," + s.byteToHex(s.setAL(s.ram[++s.eip])));
			break;
		case 0xB1:
			s.logasm("MOV " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0xB2:
			s.logasm("MOV " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0xB3:
			s.logasm("MOV BL " + s.byteToHex(s.setBL(s.ram[++s.eip])));
			break;
		case 0xB4:
			s.logasm("MOV AH " + s.byteToHex(s.setAH(s.ram[++s.eip])));
			break;
		case 0xB5:
			s.logasm("MOV " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0xB7:
			s.logasm("MOV BH," + s.byteToHex(s.setBH(s.get_i8(s.eip + 1))));
			break;
		case 0xB8:
			s.logasm("MOV AX," + s.charToHex(s.setAX(s.get_i16(s.eip + 1))));
			s.eip += opsize;
			break;
		case 0xB9:
			s.logasm("MOV CX," + s.charToHex(s.setCX(s.get_i16(s.eip + 1))));
			s.eip += opsize;
			break;
		case 0xBA:
			s.logasm("MOV DX," + s.charToHex(s.setDX(s.get_i16(s.eip + 1))));
			s.eip += opsize;
			break;
		case 0xBB:
			s.logasm("MOV BX," + s.charToHex(s.setBX(s.get_i16(s.eip + 1))));
			s.eip += opsize;
			break;
		case 0xBC:
			param = s.ram[++s.eip];
			switch(param) {
			case (byte) 0xFE:
				s.logasm("MOV SP, " + s.intToHex(s.get_i16(s.eip)));

				s.setSP(s.get_i16(s.eip++));
				break;
			default:
				s.logasm("MOV param=" + s.byteToHex(param));
				break;

			}

			break;
		case 0xBE:
			s.logasm("MOV ESI " + s.charToHex(s.setSI(s.get_i16(++s.eip))));
			++s.eip;

			break;
		case 0xBF:
			s.logasm("MOV ESI " + s.charToHex(s.setSI(s.get_i16(s.eip + 1))));
			s.eip += opsize;

			break;
		case 0xC0:
			s.logasm("rol " + s.byteToHex(s.ram[++s.eip]) + " " + s.byteToHex(s.ram[++s.eip]));
			break;
		case 0xC3:
			s.logasm("RET");
			s.eip = s.stackpop16();
			return;
		case 0xCD:
			s.logasm("INT " + s.byteToHex(s.interrupt(s.ram[++s.eip])));
			break;
		case 0x6A:

			s.logasm("PUSH " + s.get_i8(s.eip + 1));
			s.stackpush(s.get_i8(++s.eip));

			break;
		case 0xDB:
			s.logasm("DB ");
			break;
		case 0xE8:
			val = s.get(s.eip + 1, opsize);

			s.logasm("CALL " + val);
			s.eip += opsize;
			s.stackpush16((short) (s.eip + 1));

			s.eip += val;

			break;
		case 0xE9:
			tos = s.get_i16(s.eip + 1);
			s.eip += tos;
			s.logasm("JMP " + tos);
			break;
		case 0xEA:
			short to16 = s.get_i16(s.eip + 1);

			s.eip = s.startpos + to16 - 1;
			s.logasm("JMP FAR " + s.charToHex(to16));

			break;
		case 0xEB:
			to = s.get_i8(++s.eip);
			s.logasm("JMP " + to);
			s.eip += to;
			break;
		case 0xF3:
			param = s.ram[s.eip + 1];
			s.logasm("REP "+s.byteToHex(param) + " count: "+s.ECX );
			switch (param) {
				case (byte) 0xA5:
					// Move (E)CX doublewords from DS:[(E)SI] to ES:[(E)DI].
					int ES=s.ES&0xffff;
					int DS=s.DS&0xffff;
					while(--s.ECX>0){
						s.ram[ES * 16 + (s.EDI += 2)] = s.ram[DS * 16 + (s.ESI += 2)];
					}
					s.eip += 2;
					break;
			}
			return;
		case 0xF4:
			s.logasm("HLT ");
			return;
		case 0xF6:

			s.logasm("TEST " + s.byteToHex(s.ram[++s.eip]) + " " + s.byteToHex(s.ram[++s.eip]));
			s.getDL();
			s.unsetFlag(s.CF);
			s.unsetFlag(s.OF);

			break;
		case 0xFA:
			s.logasm("CLI ");
			s.cli();
			break;
		case 0xFB:
			s.logasm("STI ");
			s.setFlag(s.IF);
			break;
		case 0xFC:
			s.logasm("CLD ");
			s.unsetFlag(s.CF);
			break;

		default:
			s.logasm("nop: " + s.byteToHex(op));
			break;
		}
		s.eip++;

	}

	private void _0f(State s) {
		int op = (s.ram[++s.eip]) & 0xff;
		byte param;
		switch (op) {
		case 0xA1:
			s.logasm("POP FS");
			s.setFS(s.stackpop16());
			break;
		case 0xB5:
			param = s.ram[++s.eip];
			switch (param) {
				case 0x37:
					s.logasm("LGS SI, FS[DX]");
					int addr = s.getFS() * 16 + s.getBX();
					s.logasm("addr=" + s.intToHex(addr));

					s.setGS(s.get_i16(s.getFS() * 16 + s.getBX()));
					s.logasm("GS=" + s.getGSString());

					break;
				default:
					s.logasm("LGS: " + s.byteToHex(param));
					break;
			}

			break;
		case 0x10:
			param = s.ram[++s.eip];
			int param2 = s.ram[++s.eip];
			int param3 = s.ram[++s.eip];
			s.logasm("MOVUPS: " + s.byteToHex(param)+", "+s.byteToHex(param2)+", "+s.byteToHex(param3));
			break;
		case 0x51:
			param = s.ram[++s.eip];
			s.logasm("SQRTPS: " + s.byteToHex(param));
			break;

		default:
			s.logasm("nop: 0F " + s.byteToHex(s.ram[s.eip]));
			break;
		}

	}

	public void setScreen(Screen sc) {
		s.setScreen(sc);
	}

	public static boolean willSubtractionOverflow(char left, char right) {
		if (right < 0) {
			return willAdditionOverflow(left, (char) -right);
		} else {
			return ((left ^ right) & (left ^ (left - right))) < 0;
		}
	}

	public static boolean willAdditionOverflow(char left, char right) {
		if (right < 0 && right != Character.MIN_VALUE) {
			return willSubtractionOverflow(left, (char) -right);
		} else {
			return (~(left ^ right) & (left ^ (left + right))) < 0;
		}
	}

	public void reset() {
		// TODO Auto-generated method stub

	}
}
