package com.neoexpert.x86;


import java.io.*;
import java.nio.*;
import java.util.*;

public class State {
	public int EAX;
	public int EBX;
	public int ECX;
	public int EDX;
	public int ESI=0xE0000;

	public short CS;
	public short DS;
	public short ES;
	public short SS;
	public short FS;
	public short GS;

	public long xmm0h;
	public long xmm0l;
	public long xmm1h;
	public long xmm1l;
	public long xmm2h;
	public long xmm2l;
	public long xmm3h;
	public long xmm3l;
	public long xmm4h;
	public long xmm4l;
	public long xmm5h;
	public long xmm5l;
	public long xmm6h;
	public long xmm6l;
	public long xmm7h;
	public long xmm7l;



	public byte[] ram = new byte[1024 * 1024 * 128];
	//public int startpos = 0x7C00;
	public int startpos = 0x7c00;

	public int eip = startpos;
	public  int EBP;

	public int ESP=0xFFD6;
	public int EDI=0xFFAC;
	public int IP;
	public int GDTR;
	public int IDTR;
	public int CR0;
	public int CR2;
	public int CR4;
	public int CR3;
	public int EFER;
	//public ByteBuffer r;
	private Screen screen;

	public int EFLAGS;
	private AsmLog asmlog=null;;

	public static final int CF = 0;
	public static final int PF = 2;
	public static final int AF = 4;
	public static final int ZF = 6;
	public static final int SF = 7;
	public static final int TF = 8;
	public static final int IF = 9;
	public static final int DF = 10;
	public static final int OF = 11;
	public static final int IOPL12 = 12;
	public static final int IOPL13 = 13;
	public static final int NT = 14;
	public static final int RF = 16;
	public static final int VM = 17;
	public static final int AC = 18;
	public static final int VIF = 19;
	public static final int VIP = 20;
	public static final int ID = 21;
	private OperrandGetter og;

	private int opsize;

	
	public class OPGetter16 implements OperrandGetter
	{
		@Override
		public int get()
		{
			return get_i16(eip);
		}
	}
	private OPGetter16 og16=new OPGetter16();
	private OPGetter32 og32=new OPGetter32();
	public class OPGetter32 implements OperrandGetter
	{
		@Override
		public int get()
		{
			return get_i32(eip);
		}
	}
	public void toggleOpSize()
	{
		if(opsize==16)
			setOperandSize(32);
		else
			setOperandSize(16);
	}
	public void setOperandSize(int opsize){
		this.opsize=opsize;
		switch(opsize){
			case 16:
				og=og16;
				break;
			case 32:
				og=og32;
				break;
		}
	}
	public State(int opsize) {
		setFlag(1);
		setOperandSize(opsize);
	}
	
	public int getOperand()
	{
		return og.get();
	}

	public int getEIP()
	{
		return eip;
	}

	public int getFS()
	{
		return FS;
	}

	public int getGS()
	{
		return GS;
	}

	public int getSS()
	{
		return SS;
	}

	public int getDS()
	{
		return DS;
	}

	public int getEFLAGS()
	{
		return EFLAGS;
	}

	public int getIP()
	{
		return eip - startpos;
	}

	public int getESP()
	{
		return ESP;
	}

	public int getEBP()
	{
		return EBP;
	}

	public int getEDX()
	{
		return EDX;
	}

	public int getECX()
	{
		return ECX;
	}

	public void setFlag(int pos) {
		EFLAGS = (EFLAGS | (1 << pos));
	}

	public void unsetFlag(int pos) {
		EFLAGS = (EFLAGS & ~(1 << pos));
	}

	public String getEAXString() {
		return Integer.toHexString(EAX);
	}

	public String getEBXString() {
		return Integer.toHexString(EBX);
	}

	public String getECXString() {
		return Integer.toHexString(ECX);
	}

	public String getEDXString() {
		return Integer.toHexString(EDX);
	}

	public String getEIPString() {
		return Integer.toHexString(eip);
	}

	public String getESIString() {
		return Integer.toHexString(ESI);
	}

	public String getEDIString() {
		return Integer.toHexString(EDI);
	}

	public String getEBPString() {
		return Integer.toHexString(EBP);
	}

	public String getESPString() {
		return Integer.toHexString(ESP);
	}

	public String getIPString() {
		return Integer.toHexString(eip - startpos);
	}

	public String getEFLAGSString() {
		return Integer.toHexString(EFLAGS);
	}

	public String getCSString() {
		return Integer.toHexString(CS);
	}

	public String getDSString() {
		return Integer.toHexString(DS);
	}

	public String getESString() {
		return Integer.toHexString(ES & 0xffff);
	}

	public String getSSString() {
		return Integer.toHexString(SS);
	}

	public String getFSString() {
		return Integer.toHexString(FS);
	}

	public String getGSString() {
		return Integer.toHexString(GS);
	}

	public String getGDTRString() {
		return Integer.toHexString(GDTR);
	}

	public String getIDTRString() {
		return Integer.toHexString(IDTR);
	}

	public String getCR0String() {
		return Integer.toHexString(CR0);
	}

	public String getCR2String() {
		return Integer.toHexString(CR2);
	}

	public String getCR3String() {
		return Integer.toHexString(CR3);
	}

	public String getCR4String() {
		return Integer.toHexString(CR4);
	}

	public String getEFERString() {
		return Integer.toHexString(EFER);
	}

	public byte setAL(byte b) {
		EAX &= (byte) 0;

		int ub=b&0xff;
		EAX |= ub;

		return b;
	}

	public byte getAL() {
		return (byte) EAX;
	}

	
	public short setAX(short v) {
		EAX &= (0 & 0xFFFF);
		EAX |= (v & 0xFFFF);

		return (short) (EAX);
	}

	public short setBX(int v) {
		return (short) (EBX = v);
	}

	public short getBX() {
		return (short) EBX;
	}

	public short getAX() {
		return (short) EAX;
	}

	public byte setAH(byte b) {
		EAX |= b << 8;
		return b;
	}

	public byte setBH(byte b) {
		EBX |= b << 8;
		return b;
	}

	public byte setBL(byte b) {
		EBX |= b;
		return b;
	}

	public byte getDL() {
		return (byte) EDX;
	}

	public byte setDL(byte b) {
		EDX |= b;
		return b;
	}

	public int setESI(int v) {
		return ESI = v;
	}

	public int setESP(int v) {
		return ESP = v;
	}

	public void setSS(short v) {
		SS = v;
	}

	public int getES() {
		return ES;
	}

	public void setES(short v) {
		ES = v;
	}

	public short getCS() {
		return CS;
	}

	public void setDS(short v) {
		DS = v;
	}

	public void cli() {
		// TODO Auto-generated method stub

	}

	public void logasm(String op) {
		if (asmlog!=null)
			asmlog.log(op);
	}
	public void logasm(String op,String o1,String o2) {
		StringBuilder sb=new StringBuilder(op)
		.append(" ")
		.append(o1)
		.append(", ")
		.append(o2);
		if (asmlog!=null)
			asmlog.log(sb.toString());
	};

	public void setLog(AsmLog asmlog) {
		this.asmlog=asmlog;
	}

	public static String byteToHex(int b) {
		int v = b & 0xFF;
		char hexChar1 = hexArray[v >>> 4];
		char hexChar2 = hexArray[v & 0x0F];
		return new StringBuilder().append(hexChar1).append(hexChar2).toString();
	}

	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	// public static char twoBytesTochar(byte b1, byte b2) {
	// return (char) ((b1 & 0xFF) | ((b2 & 0xFF) << 8));
	// }

	public static String intToHex(int i) {
		return Integer.toHexString(i);
	}

	public static String charToHex(int s) {
		return Integer.toHexString(s & 0xffff);
	}
	
	public static int byteArrayToInt(byte[] ba)
	{
		int v=0;
		int o = 0;
		for (int i=0;i < ba.length;i++)
			v |= ba[i] & 0xFF << (o+=8);
		return v;
	}

	public static int fourBytesToInt(byte a, byte b, byte c, byte d)
	{
		return ((a & 0xFF) | ((b & 0xFF) << 8) | ((c & 0xFF) << 16) | ((d & 0xFF) <<24));
	}

	public int setSI(int si) {
		return ESI = si;
	}

	public int getSI() {
		return (short) ESI;
	}

	public void setGS(short v) {
		GS = v;
	}

	public void setFS(short v) {
		FS = v;
	}

	public byte interrupt(byte b) {
		switch (b) {
		case 0x10:
			videointerrupt();
			break;
		case 0x16:
			keyboardinterrupt();
			break;

		default:
			break;
		}
		return b;
	}

	private void keyboardinterrupt() {
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void videointerrupt() {
		short ch = (short) getAL();
		screen.writeChar((char)ch);
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public void setFlags(int result) {
		if (result == 0) {
			setFlag(ZF);
			unsetFlag(SF);
		} else {
			unsetFlag(ZF);
			if ((result & 0xffff) >> 15 == 1)
				setFlag(SF);
			else
				unsetFlag(SF);

		}
	}


	public int setEAX(int eax) {
		return this.EAX = eax;
	}

	public int getEBX() {
		return EBX;
	}

	public int setEBX(int i) {
		return EBX = i;
	}

	public int getEAX() {
		return EAX;
	}

	public Screen getScreen() {
		return screen;
	}

	public int setECX(int ecx) {
		return ECX = ecx;
	}

	public int setCX(int v) {
		ECX &= (0 & 0xFFFF);
		ECX |= (v & 0xFFFF);

		return (short) (ECX);
	}

	public int getESI() {
		return ESI;
	}

	public int setDI(int v) {
		EDI &= (0 & 0xFFFF);
		EDI |= (v & 0xFFFF);
		return (short) (EDI);
	}

	public int getDI() {
		return (short) EDI;
	}

	public int getEDI() {
		return EDI;
	}

	public void setEDI(int edi) {
		EDI=edi;
	}

	public short getCX() {
		return (short) ECX;
	}

	public int setDX(int dx) {
		EDX &= (0 & 0xFFFF);
		EDX |= (dx & 0xFFFF);

		return (short) (EDX);
	}

	public int setEDX(int edx) {
		return EDX=edx;
	}

	public short getDX() {
		return (short) EDX;
	}

	public int setSP(int sp) {
		ESP &= (0 & 0xFFFF);
		ESP |= (sp & 0xFFFF);
		return (short) (ESP);
	}

	public void stackpush16(short ax) {
		set_i16(ESP-=2, (short)ax);
	}
	public void stackpush32(int ax) {
		set_i32(ESP-=4,ax);
	}
	public void stackpush(byte ax) {
		set_i8(ESP--,ax);
	}
	
	public short stackpop16() {
		return get_i16(ESP+=2);
	}
	public int stackpop32() {
		return get_i32(ESP+=4);
	}
	public byte stackpop() {
		return get_i8(ESP++);
	}

	/*
	public void stackpush(byte[] a) {
		r.put(a,ESP-=a.length,a.length);
	}*/
	
	/*
	public void stackpop16(byte[] dst) {
		r.get(dst, ESP+=dst.length, dst.length);
		//return r.getChar(ESP+=2);
	}*/

	public int get(int i, int opsize) {
		switch (opsize) {
			case 1:
				return get_i8(i);
			case 2:
				return get_i16(i);
			case 4:
				return get_i32(i);
			default:
				return 0;
		}
	}

	//ram getter
	public byte get_i8(int addr){
		return ram[addr];
	}

	public short get_i16(int addr){
		return (short)((ram[addr] & 0xff)
				+ (ram[addr+1] << 8));
	}

	public int get_i32(int addr){
		return ((ram[addr] & 0xff)
				+ ((ram[addr+1] & 0xff) << 8)
				+ ((ram[addr+2] & 0xff) << 16)
				+ (ram[addr+3] << 24));
	}

	public long get_i64(int addr){
		return ((ram[addr]	 & 0xff)
				+ (((ram[addr+1] & 0xff)) << 8)
				+ (((ram[addr+2] & 0xff)) << 16)
				+ (((ram[addr+3] & 0xffL)) << 24)
				+ (((ram[addr+4] & 0xffL)) << 32)
				+ (((ram[addr+5] & 0xffL)) << 40)
				+ (((ram[addr+6] & 0xffL)) << 48)
				+ (((long)ram[addr+7]) << 56));
	}

	public void set_i8(int addr, byte v){
		ram[addr]=v;
	}

	public void set_i16(int addr, short value){
		ram[addr]=((byte) value);
		ram[addr+1]=((byte) (value >> 8));
	}

	public void set_i32(int addr, int value){
		ram[addr]=((byte) value);
		ram[addr+1]=((byte) (value >> 8));
		ram[addr+2]=((byte) (value >> 16));
		ram[addr+3]=((byte) (value >> 24));
	}

	public void set_i64(int addr, long value){
		ram[addr]=((byte) value);
		ram[addr+1]=((byte) (value >> 8));
		ram[addr+2]=((byte) (value >> 16));
		ram[addr+3]=((byte) (value >> 24));
		ram[addr+4]=((byte) (value >> 32));
		ram[addr+5]=((byte) (value >> 40));
		ram[addr+6]=((byte) (value >> 48));
		ram[addr+7]=((byte) (value >> 56));
	}
	
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append(String.format("EAX %08X", EAX));
		sb.append("\n");
		sb.append(String.format("EBX %08X", EBX));
		sb.append("\n");
		sb.append(String.format("ECX %08X", ECX));
		sb.append("\n");
		sb.append(String.format("EDX %08X", EDX));
		sb.append("\n");
		sb.append(String.format("ESI %08X", ESI));
		sb.append("\n");
		sb.append(String.format("CS %04X", CS));
		sb.append("\n");
		sb.append(String.format("DS %04X", DS));
		sb.append("\n");
		sb.append(String.format("ES %04X", ES));
		sb.append("\n");
		sb.append(String.format("SS %04X", SS));
		sb.append("\n");
		sb.append(String.format("FS %04X", FS));
		sb.append("\n");
		sb.append(String.format("GS %04X", GS));
		sb.append("\n");
		return sb.toString();
	}
}
